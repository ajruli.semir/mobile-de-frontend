import React from "react";

// Styles
import "./style.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <span className="copyright">&copy; mobile.de by Semir Ajruli</span>
      </div>
    </footer>
  );
};

export default Footer;
