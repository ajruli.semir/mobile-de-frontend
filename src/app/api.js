import axios from 'axios'

const apiURL = 'http://localhost:4000/api/v1'

const createCarListing = payload => {
  return axios.post(`${apiURL}/listings`, payload)
}
const findAllCarListings = () => {
  return axios.get(`${apiURL}/listings`)
}

const findCarListingById = listingId => {
  return axios.get(`${apiURL}/listings/${listingId}`)
}

export { createCarListing, findAllCarListings, findCarListingById }
