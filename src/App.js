import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { createCarListing, findAllCarListings } from './app/api'

// Styles
import './app.scss'

//Components
import Header from './components/Header'
import Footer from './components/Footer'
import Modal from './components/Modal'

import DataTable from './components/DataTable'
import CreateListing from './components/CreateListing'
import ViewListing from './components/ViewListing'

import Loader from './components/Loader'
import MySwal from './index'

function App () {
  const dispatch = useDispatch()
  const listings = useSelector(state => state.listings)

  const [loading, setLoading] = useState(false)

  const [currentListing, setCurrentListing] = useState({
    make: '',
    model: '',
    buildYear: '',
    description: '',
    price: '',
    email: '',
    imageUrl: ''
  })

  const [activeModal, setActiveModal] = useState({ name: '', active: false })
  // eslint-disable-next-line
  const [savedListings, setSavedListings] = useState(listings)

  // Setting up Modal
  const setModal = modal => {
    setActiveModal({ name: modal, active: true })
  }

  const updateRow = listing => {
    setModal('View Listing')
    debugger
    setCurrentListing({
      make: listing.make,
      model: listing.model,
      buildYear: listing.buildYear,
      description: listing.description,
      price: listing.price,
      email: listing.email,
      imageUrl: listing.imageUrl
    })
  }

  const createListing = async listing => {
    setActiveModal(false)
    setLoading(true)

    try {
      await createCarListing(listing).then(res => {
        const result = res.data

        MySwal.fire({
          icon: 'success',
          title: 'Listing created successfully.'
        }).then(() => {
          dispatch({ type: 'CREATE_LISTING', data: result })
          setSavedListings([...listings, result])
        })
      })
    } catch (err) {
      MySwal.fire({
        icon: 'error',
        title: 'Failed to create listing.'
      })
    } finally {
      setLoading(false)
    }
  }

  // Get All Listings
  const fetchListings = async () => {
    setLoading(true)

    try {
      await findAllCarListings().then(data => {
        setSavedListings(data)
        dispatch({ type: 'SET_LISTINGS', data: data.data })
      })
    } catch (err) {
      MySwal.fire({
        icon: 'error',
        title: 'Failed to fetch car listings.'
      })
    } finally {
      setTimeout(() => {
        setLoading(false)
      }, 500)
    }
  }

  useEffect(() => {
    fetchListings()
  }, [])

  return (
    <div className='app'>
      <Header />
      <main className='content'>
        <div className='container'>
          {loading ? (
            <Loader />
          ) : (
            <div className='content-wrapper'>
              <div className='toolbar'>
                {/* <Search search={search} resetSearch={search} /> */}
                <button
                  className='primary-btn'
                  onClick={() => setModal('Create Listing')}
                >
                  Create New Listing
                </button>
              </div>
              <DataTable listings={listings} updateRow={updateRow} />
            </div>
          )}
        </div>
      </main>
      {activeModal.active && (
        <Modal activeModal={activeModal}>
          {activeModal.name === 'Create Listing' && (
            <CreateListing
              createListing={createListing}
              setActiveModal={setActiveModal}
            />
          )}
          {activeModal.name === 'View Listing' && (
            <ViewListing
              currentListing={currentListing}
              setActiveModal={setActiveModal}
            />
          )}
        </Modal>
      )}

      <Footer />
    </div>
  )
}

export default App
