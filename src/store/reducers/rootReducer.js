const initialState = {
  listings: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_LISTINGS":
      return { ...state, listings: action.data };
    case "CREATE_LISTING":
      return { ...state, listings: [...state.listings, action.data] };
    default:
      return state;
  }
};

export default rootReducer;
