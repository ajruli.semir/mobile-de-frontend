import React, { useState } from 'react'

const CreateListing = props => {
  const [listing, setListing] = useState({})

  const onInputChange = event => {
    const { name, value } = event.target

    setListing({ ...listing, [name]: value })
  }

  const cancel = event => {
    event.preventDefault()
    props.setActiveModal({ active: false })
  }

  return (
    <form
      onSubmit={event => {
        event.preventDefault()

        if (
          !listing.make ||
          !listing.model ||
          !listing.buildYear ||
          !listing.description ||
          !listing.price ||
          !listing.email
        )
          return

        props.createListing(listing)
      }}
    >
      <div className='form-group'>
        <label>Make * </label>
        <input
          type='text'
          name='make'
          value={listing.make}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>Model *</label>
        <input
          type='text'
          name='model'
          value={listing.model}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>Build Year * </label>
        <input
          type='text'
          name='buildYear'
          value={listing.buildYear}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>Description *</label>
        <textarea
          name='description'
          value={listing.description}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>Price *</label>
        <input
          type='text'
          name='price'
          value={listing.price}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>E-mail * </label>
        <input
          type='email'
          name='email'
          value={listing.emial}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group'>
        <label>Image url </label>
        <input
          type='text'
          name='imageUrl'
          value={listing.imageUrl}
          onChange={onInputChange}
        />
      </div>
      <div className='form-group form-group--actions'>
        <button className='primary-btn'>Create</button>
        <button className='cancel-btn' onClick={cancel}>
          Cancel
        </button>
      </div>
    </form>
  )
}

export default CreateListing
