import React, { useState, useEffect } from 'react'
import PlaceholderImg from '../../img/placeholder-car.jpg'

const ViewListing = props => {
  const [listing, setListing] = useState(props.currentListing)

  debugger

  const cancel = event => {
    event.preventDefault()
    props.setActiveModal({ active: false })
  }

  useEffect(() => {
    setListing(props.currentListing)
  }, [props])

  return (
    <div>
      <div className='gallery'>
        <a target='_blank' href=''>
          <img
            src={listing.imageUrl ? listing.imageUrl : PlaceholderImg}
            alt={listing.make}
          />
        </a>
        <div className='desc'>
          <table>
            <tbody>
              <tr>
                <td>Email:</td>
                <td>{listing.email}</td>
              </tr>
              <tr>
                <td>Make:</td>
                <td>{listing.make}</td>
              </tr>
              <tr>
                <td>Model:</td>
                <td>{listing.model}</td>
              </tr>
              <tr>
                <td>Price:</td>
                <td>{listing.price}</td>
              </tr>
              <tr></tr>
              <tr>
                <td>Build Year:</td>
                <td>{listing.buildYear}</td>
              </tr>
              <tr>
                <td>Description:</td>
                <td>{listing.description}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className='form-group form-group--actions'>
        <button className='cancel-btn' onClick={cancel}>
          Cancel
        </button>
      </div>
    </div>

    // <div>
    //   <div className='form-group'>
    //     <label>Make {listing.make}</label>
    //   </div>
    //   <div className='form-group'>
    //     <label>Model</label>
    //   </div>
    //   <div className='form-group'>
    //     <label>Build Year </label>
    //   </div>
    //   <div className='form-group'>
    //     <label>Description</label>
    //   </div>
    //   <div className='form-group'>
    //     <label>Price</label>
    //   </div>
    //   <div className='form-group'>
    //     <label>E-mail </label>
    //   </div>
    //   <div className='form-group'>
    //     <label>Image url</label>
    //   </div>
    //   <div className='form-group form-group--actions'>
    //     <button className='cancel-btn' onClick={cancel}>
    //       Cancel
    //     </button>
    //   </div>
    // </div>
  )
}

export default ViewListing
