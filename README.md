# Mobile De Frontend

## Prerequisites

- Node 14
## Development

1. Install dependencies

```shell
$ npm i
```

2. Start service

```
$ npm run dev

```
