import React from 'react'
import './style.scss'
import PlaceholderImg from '../../img/placeholder-car.jpg'

const DataTable = props => {
  return (
    <div className='table-wrapper'>
      <table className='data-table'>
        <thead>
          <tr>
            <th>Image</th>
            <th>
              <span>Make</span>
            </th>
            <th>
              <span>Model</span>
            </th>
            <th>
              <span>Price</span>
            </th>
          </tr>
        </thead>
        <tbody>
          {props.listings.length ? (
            props.listings.map(listing => (
              <tr key={listing.id}>
                <td className='field-avatar'>
                  <img
                    src={listing.imageUrl ? listing.imageUrl : PlaceholderImg}
                    alt={listing.make}
                  />
                </td>
                <td>{listing.make}</td>
                <td>{listing.model}</td>
                <td>{listing.price} €</td>
                <td className='field-actions'>
                  <button
                    className='primary-btn'
                    onClick={() => {
                      props.updateRow(listing)
                    }}
                  >
                    Details
                  </button>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan='5'>
                <div className='no-record-message'>No Record</div>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

export default DataTable
